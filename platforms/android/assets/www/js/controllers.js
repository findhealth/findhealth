angular.module('findhealth.controllers', ['ionic'])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig) {

})

//LOGIN
.controller('LoginCtrl', function($scope, $state, $templateCache, $q, $rootScope) {
     
   
	$scope.doLogIn = function(){
		$state.go('app.home');
	};

	$scope.user = {};

	$scope.user.email = "gabriel@findhealth.com";

	// We need this for the form validation
	$scope.selected_tab = "";

	$scope.$on('my-tabs-changed', function (event, data) {
		$scope.selected_tab = data.title;
	});

})

.controller('SignupCtrl', function($scope, $state) {
	$scope.user = {};

	$scope.user.email = "";

	$scope.doSignUp = function(){
		$state.go('app.home');
	};
})

.controller('ForgotPasswordCtrl', function($scope, $state) {
	$scope.recoverPassword = function(){
		$state.go('app.home');
	};

	$scope.user = {};
})

.controller('ctrl', function($scope) {
  $scope.registerData = {};
})

// Home
.controller('HomeCtrl', function($scope, $http) {
   $scope.registerData = {};
})

;

